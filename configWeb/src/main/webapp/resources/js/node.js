var contextPath = $("script").last().attr("src").replace("/js/node.js","");
$(function () {
	$('#tree').jstree({ 
		'core' : {
			'data' : {
				'url' : contextPath+"/read/node?path="+encodeURIComponent($("#zkpath").val())
			},
			'check_callback' : function(o, n, p, i, m) {
				if(m && m.dnd && m.pos !== 'i') { return false; }
				if(o === "move_node" || o === "copy_node") {
					if(this.get_node(n).parent === this.get_node(p).id) { return false; }
				}
				return true;
			}
		},
		'contextmenu' : {
			'items' : function(node) {
				var tmp = $.jstree.defaults.contextmenu.items();
				tmp.create.label = '创建';
				tmp.rename.label = '重命名';
				tmp.remove.label = '删除';
				tmp.ccp.label = '编辑';
				tmp.ccp.submenu.cut.label = '剪切';
				tmp.ccp.submenu.copy.label = '复制';
				tmp.ccp.submenu.paste.label = '粘贴';
				return tmp;
			}
		},
		'plugins' : ['dnd','contextmenu','sort','search']
	}).on('select_node.jstree', function (e, data) {
		//alert('The selected node is: ' + data.instance.get_node(data.selected[0]).text);
		if(data && data.selected && data.selected.length) {
			window.document.body.style.cursor='wait';
			var names = data.instance.get_path(data.selected[0],"/").replace(/^\/+/,"/");
			loadRight(names);
			window.document.body.style.cursor='default';
		}
	}).on('create_node.jstree', function (e, data) {
		var loginStatus = $("#loginStatus").val();
		if(loginStatus==0) {
			$("#loginModal").modal({keyboard:true});
			data.instance.refresh();
			return;
		}
		if(loginStatus!=1) {
			alert("你没有权限创建节点,请与管理员联系");
			data.instance.refresh();
			return;
		}
		window.document.body.style.cursor='wait';
		var parent = data.instance.get_path(data.node.parent,"/").replace(/^\/+/,"/");
		var name = data.node.text;
		//alert("create_node=parent : "+parent+", 'name' : "+name);
		$.ajax({
			cache: true,
			type: "POST",
			url:contextPath+"/op/createNode",
			data:{parent:parent,name:name},
			async: false,
			error: function(request) {
				alert("Connection error");
				data.instance.refresh();
				window.document.body.style.cursor='default';
			},
			success: function(result) {
				if(result) {
					var djson=eval(result);
					if(djson && djson.state == 0) {
						window.document.body.style.cursor='default';
					} else {
						alert(djson.msg);
						data.instance.refresh();
						window.document.body.style.cursor='default';
					}
				}
			}
		});
	}).on('delete_node.jstree', function (e, data) {
		var loginStatus = $("#loginStatus").val();
		if(loginStatus==0) {
			$("#loginModal").modal({keyboard:true});
			data.instance.refresh();
			return;
		}
		if(loginStatus!=1) {
			alert("你没有权限删除节点,请与管理员联系");
			data.instance.refresh();
			return;
		}
		var path = data.instance.get_path(data.node.id,"/").replace(/^\/+/,"/");
		var parent = data.instance.get_path(data.node.parent,"/").replace(/^\/+/,"/");
		//alert("delete_node=path : "+encodeURIComponent(path) + ", parent="+parent);
		if (confirm("你确定要删除"+path+"节点和此节点下面的所有子节点吗?")) {
			window.document.body.style.cursor='wait';
			$.ajax({
				cache: true,
				type: "POST",
				url:contextPath+"/op/deleteNode",
				data:{path:path},
				async: false,
				error: function(request) {
					alert("Connection error");
					data.instance.refresh();
					window.document.body.style.cursor='default';
				},
				success: function(result) {
					if(result) {
						var djson=eval(result);
						if(djson && djson.state == 0) {
							loadRight(parent);
							window.document.body.style.cursor='default';
						} else {
							alert(djson.msg);
							data.instance.refresh();
							window.document.body.style.cursor='default';
						}
					}
				}
			});
		} else {
			data.instance.refresh();
		}
	}).on('rename_node.jstree', function (e, data) {
		var loginStatus = $("#loginStatus").val();
		if(loginStatus==0) {
			$("#loginModal").modal({keyboard:true});
			data.instance.refresh();
			return;
		}
		if(loginStatus!=1) {
			alert("你没有权限重命名节点,请与管理员联系");
			data.instance.refresh();
			return;
		}
		var parent = data.instance.get_path(data.node.parent,"/");
		if(parent) {
			parent = parent + "/";
		} else {
			parent = "/";
		}

		var newPath = (parent + data.text).replace(/^\/+/,"/");
		var oldPath = (parent + data.old).replace(/^\/+/,"/");
		//alert("rename_node=newPath : "+newPath+",oldPath="+oldPath);
		if(oldPath == newPath || oldPath == "/") {
			data.instance.refresh();
			return;
		}
		window.document.body.style.cursor='wait';
		$.ajax({
			cache: true,
			type: "POST",
			url: contextPath+"/op/renameNode",
			data: {oldPath:oldPath,newPath:newPath},
			async: false,
			error: function(request) {
				alert("Connection error");
				data.instance.refresh();
				window.document.body.style.cursor='default';
			},
			success: function(result) {
				if(result) {
					var djson=eval(result);
					if(djson && djson.state == 0) {
						setPath(newPath);
						window.document.body.style.cursor='default';
					} else {
						alert(djson.msg);
						data.instance.refresh();
						window.document.body.style.cursor='default';
					}
				}
			}
		});
	}).on('move_node.jstree', function (e, data) {
		var loginStatus = $("#loginStatus").val();
		if(loginStatus==0) {
			$("#loginModal").modal({keyboard:true});
			data.instance.refresh();
			return;
		}
		if(loginStatus!=1) {
			alert("你没有权限移动节点,请与管理员联系");
			data.instance.refresh();
			return;
		}
		window.document.body.style.cursor='wait';
		var targetPath = data.instance.get_path(data.parent,"/").replace(/^\/+/,"/");
		var path;
		if(data.old_parent == "#") {
			path = data.node.text;
		} else {
			path = data.instance.get_path(data.old_parent,"/") + "/" + data.node.text;
		}
		path = path.replace(/^\/+/,"/");

		//alert("move_node=path : "+path+",targetPath="+targetPath);
		$.ajax({
			cache: true,
			type: "POST",
			url: contextPath+"/op/cutPasteNode",
			data: {path:path,targetPath:targetPath},
			async: false,
			error: function(request) {
				alert("Connection error");
				data.instance.refresh();
				window.document.body.style.cursor='default';
			},
			success: function(result) {
				if(result) {
					var djson=eval(result);
					if(djson && djson.state == 0) {
						data.instance.refresh();
						window.document.body.style.cursor='default';
					} else {
						alert(djson.msg);
						data.instance.refresh();
						window.document.body.style.cursor='default';
					}
				}
			}
		});
	}).on('copy_node.jstree', function (e, data) {
		var loginStatus = $("#loginStatus").val();
		if(loginStatus==0) {
			$("#loginModal").modal({keyboard:true});
			data.instance.refresh();
			return;
		}
		if(loginStatus!=1) {
			alert("你没有权限复制节点,请与管理员联系");
			data.instance.refresh();
			return;
		}
		window.document.body.style.cursor='wait';
		var targetPath = data.instance.get_path(data.parent,"/").replace(/^\/+/,"/");
		var path = data.instance.get_path(data.original.id,"/").replace(/^\/+/,"/");
		//alert("copy_node=path : "+path+",targetPath="+targetPath);
		$.ajax({
			cache: true,
			type: "POST",
			url: contextPath+"/op/copyPasteNode",
			data: {path:path,targetPath:targetPath},
			async: false,
			error: function(request) {
				alert("Connection error");
				data.instance.refresh();
				window.document.body.style.cursor='default';
			},
			success: function(result) {
				if(result) {
					var djson=eval(result);
					if(djson && djson.state == 0) {
						window.document.body.style.cursor='default';
					} else {
						alert(djson.msg);
						data.instance.refresh();
						window.document.body.style.cursor='default';
					}
				}
			}
		});
	});

	var to = false;
	$("#searchNode").keyup(function () {
		if(to) { clearTimeout(to); }
		to = setTimeout(function () {
			var v = $("#searchNode").val();
			$("#tree").jstree(true).search(v);
		}, 500);
	});
});

function loadRight(path) {
	setPath(path);
	var srcValue=contextPath+"/read/getData?path="+encodeURIComponent(path);
	$("#rightContent").css("display","block");
	$("#rightContent").load(srcValue);
}

function setPath(path) {
	$("#backuppath").text(path);
	$("#pathAppend").text(path);
	$("#uploadpath").text(path);
	$("#downloadpath").text(path);
	$("#createpath").text(path);
	$("#editpath").text(path);
	$("#deletepath").text(path);
	$("#rmrpath").text(path);
	$("#createpath2").val(path);
	var namepath = $("input[name='path']");
	for(var i = 0; i < namepath.length; i++) {
		$(namepath[i]).val(path);
	}
}

function deleteBackupFile(index, fileName) {
	if(confirm("你确定要删除叫"+fileName+"的备份文件吗?")) {
		window.document.body.style.cursor='wait';
		$.ajax({
			cache: true,
			type: "POST",
			url: contextPath+"/op/deleteBackupFile",
			data: {fileName:fileName},
			async: false,
			error: function(request) {
				alert("Connection error");
				window.document.body.style.cursor='default';
			},
			success: function(result) {
				if(result) {
					var djson=eval(result);
					if(djson && djson.state == 0) {
						$("#bfiletr"+index).remove();
						window.document.body.style.cursor='default';
					} else {
						alert(djson.msg);
						window.document.body.style.cursor='default';
					}
				}
			}
		});
	}
}

function closeCache() {
	if(confirm("你确定要关闭缓存加速吗?")) {
		window.document.body.style.cursor='wait';
		$.ajax({
			cache: true,
			type: "POST",
			url: contextPath+"/read/closeCache",
			data: "",
			async: false,
			error: function(request) {
				alert("Connection error");
				window.document.body.style.cursor='default';
			},
			success: function(result) {
				if(result) {
					var djson=eval(result);
					if(djson && djson.state == 0) {
						$("#closeCacheDiv").hide();
						$("#openCacheDiv").show();
						alert("缓存加速已关闭");
						window.document.body.style.cursor='default';
					} else {
						alert(djson.msg);
						window.document.body.style.cursor='default';
					}
				}
			}
		});
	}
}

function openCache() {
	window.document.body.style.cursor='wait';
	$.ajax({
		cache: true,
		type: "POST",
		url: contextPath+"/read/openCache",
		data: "",
		async: false,
		error: function(request) {
			alert("Connection error");
			window.document.body.style.cursor='default';
		},
		success: function(result) {
			if(result) {
				var djson=eval(result);
				if(djson && djson.state == 0) {
					$("#openCacheDiv").hide();
					$("#closeCacheDiv").show();
					alert("已开启缓存加速，");
					window.document.body.style.cursor='default';
				} else {
					alert(djson.msg);
					window.document.body.style.cursor='default';
				}
			}
		}
	});
}
package com.zk.config.web.model;

import lombok.Data;

@Data
public class User {
	private String name;
	private String password;
	private int role;
}

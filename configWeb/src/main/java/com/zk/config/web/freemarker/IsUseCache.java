package com.zk.config.web.freemarker;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.zk.config.web.constants.Constants;
import com.zk.config.web.op.MyZkClient;
import com.zk.config.web.op.Zk;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

public class IsUseCache implements TemplateMethodModelEx {

	@SuppressWarnings("rawtypes")
	@Override
	public Object exec(List arg0) throws TemplateModelException {
		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		String cxnstr = (String) req.getSession().getAttribute(Constants.CX_STR);
		if (StringUtils.isBlank(cxnstr)) return 0;
		Zk zk = new Zk(cxnstr);
		MyZkClient client = (MyZkClient)zk.getClient();
		if(client == null) return 0;
		return client.useCache?1:0;
	}

}

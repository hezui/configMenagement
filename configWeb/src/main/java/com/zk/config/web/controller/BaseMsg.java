package com.zk.config.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Data;

@Data
public class BaseMsg{
	private List<String> normalMsg = new ArrayList<>();
	private List<String> errMsg = new ArrayList<>();
	private Map<String, String> fieldErrMsg = new HashMap<>();
	
	public void addNormalMsg(String msg){
		normalMsg.add(msg);
	}
	
	public void addErrMsg(String msg){
		errMsg.add(msg);
	}
	
	public void addFieldErrMsg(String key, String msg){
		fieldErrMsg.put(key, msg);
	}
}

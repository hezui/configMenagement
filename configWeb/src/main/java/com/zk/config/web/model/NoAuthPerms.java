package com.zk.config.web.model;

import java.util.List;

import lombok.Data;

@Data
public class NoAuthPerms {
	private String path;
	private List<ZkPerms> userPerms;
}

package com.zk.config.web.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class TreeData {
	private String text; // 路径
	private String icon = "folder"; // 图标样式
	private List<TreeData> children; // 子节点
	private Map<String, String> state; // 节点树的标识状态
	private ZkData data; // 内容
	
	public TreeData clone() {
		TreeData td = new TreeData();
		td.setText(text);
		td.setIcon(icon);
		if(children != null) {
			List<TreeData> children2 = new ArrayList<>();
			for(TreeData ctd:children) {
				children2.add(ctd.clone());
			}
			td.setChildren(children2);
		}
		if(state != null) {
			Map<String, String> state2 = new HashMap<>();
			state2.putAll(state);
			td.setState(state2);
		}
		if(data != null) {
			td.setData(data.clone());
		}
		return td;
	}
	
	public void clear(){
		text = null;
		icon = null;
		if(children != null) {
		    for(TreeData td:children) {
		        td.clear();
		        td = null;
		    }
		    children.clear();
		    children = null;
		}
		if(state != null) {
		    state.clear();
		    state = null;
		}
		if(data != null) {
		    data.clear();
		    data = null;
		}
	}
}

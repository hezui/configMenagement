package com.zk.config.web.model;

import lombok.Data;

@Data
public class ZkPerms {
	private String perms;
	private String user;
}

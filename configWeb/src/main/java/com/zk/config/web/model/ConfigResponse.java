package com.zk.config.web.model;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ConfigResponse extends Response {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8686215927177567093L;
	private long time = 0;
	private String foundPath = "";
	private List<NoAuthPerms> noAuthPaths;
	private Param params;
}

package com.zk.config.web.model;

import java.util.Arrays;
import org.apache.zookeeper.data.Stat;

public class ZkData {

   private byte[] data;
   private Stat stat;
   
   public void clear() {
	   data = null;
	   stat = null;
   }
   
   public ZkData clone() {
	   ZkData zkData = new ZkData();
	   if(data != null) {
		   zkData.setData(data.clone());
	   }
	   if(stat != null) {
		   Stat stat2 = new Stat();
		   stat2.setAversion(stat.getAversion());
		   stat2.setCtime(stat.getCtime());
		   stat2.setCversion(stat.getCversion());
		   stat2.setCzxid(stat.getCzxid());
		   stat2.setDataLength(stat.getDataLength());
		   stat2.setEphemeralOwner(stat.getEphemeralOwner());
		   stat2.setMtime(stat.getMtime());
		   stat2.setMzxid(stat.getMzxid());
		   stat2.setNumChildren(stat.getNumChildren());
		   stat2.setPzxid(stat.getPzxid());
		   stat2.setVersion(stat.getVersion());
		   zkData.setStat(stat2);
	   }
	   return zkData;
   }

   public byte[] getBytes() {
      return data;
   }

   @Override
   public String toString() {
      return "ZkData [data=" + Arrays.toString(getData()) + ",stat=" + getStat() + "]";
   }

   public String getDataString() {
	   if(getData() != null)
			return new String(getData());
	   return null;
   }
   
   public byte[] getData() {
      return data;
   }

   public void setData(byte[] data) {
      this.data = data;
   }

   public Stat getStat() {
      return stat;
   }

   public void setStat(Stat stat) {
      this.stat = stat;
   }
}
